docker-make
===========

A Docker image that contains just the basic tools to build other Dockers with Makefile
or scripts. It's main usage is within a continuous integration script.


Downloading
-----------

:docker:`cappysan/docker-make` can be downloaded from GitLab:

.. code-block:: bash

  docker pull registry.gitlab.com/cappysan/images/docker-make


Contents
--------

This docker inherits from :docker:`cappysan/make` and adds:

* :em:`docker` to be able to build and run images


Usage
-----

An example of :docker:`cappysan/docker-make` usage in a GitLab CI:

.. code-block:: yaml

  job:
    stage: build
    image: registry.gitlab.com/cappysan/images/docker-make
    services:
      - name: docker:dind
        alias: docker
    variables:
      FF_NETWORK_PER_BUILD: "true"
      DOCKER_TLS_CERTDIR: ""
      DOCKER_HOST: "tcp://docker:2375"
    script:
      - docker build -t example:latest .
