make
====

A Docker image that contains just the basic tools to build other Dockers with Makefile
or scripts. It's main usage is within a continuous integration script.


Downloading
-----------

:docker:`cappysan/make` can be downloaded from GitLab:

.. code-block:: bash

  docker pull registry.gitlab.com/cappysan/images/make


Contents
--------

This docker inherits from :docker:`alpine` and adds:

* :em:`bash` to have a shell other than busybox
* :em:`curl` to be able to trigger webhooks
* :em:`gettext` to have envsubst to alter Dockerfiles on the fly (without using Docker build-args)
* :em:`git` to be able to read current repository
* :em:`gron` to be able to parse a JSON response
* :em:`jq` to be able to parse a JSON response
* :em:`make` to enable building Dockerfiles via Makefiles
* :em:`rsync` to be able to handle remote file repositories and merging


Usage
-----

An example of :docker:`cappysan/make` usage in a GitLab CI:

.. code-block:: yaml

  job:
    image: registry.gitlab.com/cappysan/images/make
    stage: build
    script:
      - make check
