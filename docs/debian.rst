debian
======

A Docker image that contains just the basic tools for running Debian tests in a CI/CD or even
in a ``molecule`` test environment.


Downloading
-----------

:docker:`cappysan/debian` can be downloaded from GitLab:

.. code-block:: bash

  docker pull registry.gitlab.com/cappysan/images/debian:11
  docker pull registry.gitlab.com/cappysan/images/debian:12


Contents
--------

This docker inherits from :docker:`debian` and adds:

* :em:`curl` to be able to trigger webhooks
* :em:`gettext` to have envsubst to alter Dockerfiles on the fly (without using Docker build-args)
* :em:`git` to be able to read current repository
* :em:`gron` to be able to parse a JSON response
* :em:`jq` to be able to parse a JSON response
* :em:`make` to enable building Dockerfiles via Makefiles
* :em:`moreutils` to have tools such as ``sponge``
* :em:`pipx` to be able to install Python packages
* :em:`python3-venv` to be able to install Python packages
* :em:`rsync` to be able to handle remote file repositories and merging
* :em:`systemctl` to be able to run mock systemctl commands in ``molecule``


Usage
-----

An example of :docker:`cappysan/debian` usage in an :file:`molecule.yml` file:

.. code-block:: yaml

  platforms:
    - name: instance
      image: registry.gitlab.com/cappysan/images/debian:11
      pre_build_image: false
