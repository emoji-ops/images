python-make
===========

A Docker image that contains just the basic tools to build other Dockers with Makefile
or scripts. It's main usage is within a continuous integration script.


Downloading
-----------

:docker:`cappysan/python-make` can be downloaded from GitLab:

.. code-block:: bash

  docker pull registry.gitlab.com/cappysan/images/python-make


Contents
--------

This docker inherits from :docker:`cappysan/make` and adds:

* :em:`gitlabsolute` to be able to interact with GitLab
* :em:`pipx` to be able to install Python packages
* :em:`py3-virtualenv` to be able to create virtual environments
* :em:`python-gitlab` to be able to interact with GitLab
* :em:`python3` to run Python scripts


Usage
-----

An example of :docker:`cappysan/python-make` usage in a GitLab CI:

.. code-block:: yaml

  job:
    image: registry.gitlab.com/cappysan/images/python-make
    stage: build
    script:
      - python -mvenv .venv
      - source .venv/bin/activate
      - make check
