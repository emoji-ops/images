GitLab CI images
================

**GitLab CI images** is a comprehensive **GitLab** CI/CD Docker image repository.

.. toctree::
   :maxdepth: 1
   :caption: Contents

   make.rst
   python-make.rst
   docker-make.rst
   python.rst
   debian.rst

.. toctree::
   :maxdepth: 1
   :caption: Miscellaneous

   license.rst


Versioning
----------

This project is only available as master for git, and latest for docker.


Locations
---------

* GitLab: `https://gitlab.com/cappysan/images`_

.. _https://gitlab.com/cappysan/images: https://gitlab.com/cappysan/images
