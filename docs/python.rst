python
======

A Docker image that contains just the basic tools with a Python environment. It's main usage is within a continuous integration script.


Downloading
-----------

:docker:`cappysan/python` can be downloaded from GitLab:

.. code-block:: bash

  docker pull registry.gitlab.com/cappysan/images/python:3.6
  docker pull registry.gitlab.com/cappysan/images/python:3.7
  docker pull registry.gitlab.com/cappysan/images/python:3.8
  docker pull registry.gitlab.com/cappysan/images/python:3.9
  docker pull registry.gitlab.com/cappysan/images/python:3.10
  docker pull registry.gitlab.com/cappysan/images/python:3.11
  docker pull registry.gitlab.com/cappysan/images/python:3.12


Contents
--------

This docker inherits from :docker:`python:3.x-slim` and adds:

* :em:`curl` to be able to trigger webhooks
* :em:`gettext` to have envsubst to alter Dockerfiles on the fly (without using Docker build-args)
* :em:`docker-systemctl-replacement` as a SystemD systemctl replacement
* :em:`git` to be able to read current repository
* :em:`gron` to be able to parse a JSON response
* :em:`jq` to be able to parse a JSON response
* :em:`make` to enable building Dockerfiles via Makefiles
* :em:`moreutils` to have tools such as ``sponge``
* :em:`pipx` to be able to install Python packages
* :em:`pre-commit` preinstalled for Python jobs
* :em:`python3-tomli` to be able to read :file:`pyproject.toml`
* :em:`python3-venv` to be able to install Python packages
* :em:`rsync` to be able to handle remote file repositories and merging
* :em:`sphinx`, and dependencies, preinstalled for Python jobs
* :em:`yq` to be able to parse a YAML files


Status
------

The status of the Docker image depends on the underlying Python version. Refer to: https://devguide.python.org/versions/

.. list-table:: Title
   :widths: 50 50
   :header-rows: 1

   * - image
     - status
   * - python:3.6
     - deprecated
   * - python:3.7
     - deprecated
   * - python:3.8
     - end-of-life: 2024-10
   * - python:3.9
     - end-of-life: 2025-10
   * - python:3.10
     - end-of-life: 2026-10
   * - python:3.11
     - end-of-life: 2027-10
   * - python:3.12
     - end-of-life: 2028-10


Usage
-----

An example of :docker:`cappysan/python:3.x` usage in a GitLab CI:

.. code-block:: yaml

  job:
    image: registry.gitlab.com/cappysan/images/python:3.11
    stage: build
    script:
      - make check
