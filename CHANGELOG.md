# Changelog

All notable changes to this project will be documented in this file.

## Unreleased

### Added

- n/a

### Changed
- n/a

### Deprecated
- n/a

### Removed
- n/a

### Fixed
- n/a

## 2025-02-10

### Changed

- Use UTC for date in `org.opencontainers.image.created` LABEL.
- Fixed missing LABELs, and urls in LABELs.

## 2025-02-03

### Added

- Added a CHANGELOG.md file.
