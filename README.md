# GitLab CI images

**GitLab CI images** is a comprehensive **GitLab** CI/CD Docker image repository.

The following docker images are available and are documented:

* `cappysan/make:` GNU Make and tools in Alpine
* `cappysan/python-make:` Python3 and tools built on top of `cappysan/make`
* `cappysan/docker-make:` Docker and tools built on top of `cappysan/make`
* `cappysan/python:` Debian Python3 environment with tools on top
* `cappysan/debian:` Debian environment with tools on top


## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details.


## Locations

  * Documentation: [https://images.docs.cappysan.dev](https://images.docs.cappysan.dev)
  * Website: [https://gitlab.com/cappysan/images/](https://gitlab.com/cappysan/images/)
