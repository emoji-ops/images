CI_REGISTRY     ?= registry.gitlab.com
CI_PROJECT_PATH ?= cappysan/images
CI_IMAGE_PATH   ?= $(CI_REGISTRY)/$(CI_PROJECT_PATH)

# Docker variables
DOCKER_TAG  ?= $(shell git rev-parse --abbrev-ref HEAD 2>/dev/null)
DOCKERFLAGS ?=

.PHONY: all
all: docker

.PHONY: docker
docker: $(shell LC_ALL=C ls -1 docker/ | sed "s/^/docker\//")

# Metadata is present in the Dockerfile
.PHONY: docker/%
docker/%: docker/%/Dockerfile FORCE
	docker buildx build -f $< \
	  --label org.opencontainers.image.created="$(shell TZ=UTC date -Iseconds)" \
	  --build-arg CI_IMAGE_PATH=$(CI_IMAGE_PATH) \
	  --tag ${CI_IMAGE_PATH}/$(shell grep -F "CI_IMAGE_NAME=" $< | cut -f2 -d"=") \
	  $(DOCKERFLAGS) .

.PHONY: push
push: $(shell ls -1 docker/ | sed "s/^/push\//")

.PHONY: push/%
push/%: docker/%/Dockerfile docker/% FORCE
	docker push ${CI_IMAGE_PATH}/$(shell grep -F "CI_IMAGE_NAME=" $< | cut -f2 -d"=")
